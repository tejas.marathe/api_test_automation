/**
 * 
 */
package com.abacus.dummyapi;

import static io.restassured.RestAssured.given;

import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.path.json.exception.JsonPathException;


/**
 * @author tejas_marathe
 * This class contains test cases for Create operations on Dummy API
 * Following tests are covered:
 * Positive Tests
 * 1. Create Records with following data
 *    - Typical/Average Values for Name, Salary, Age fields
 *    - Minimum values for Age, Salary
 *    - Maximum/Very high values for Age, Salary
 *    - Floating point value for Salary
 *    - Name values containing spaces, special characters, accent marks, punctuation marks
 * Negative Tests
 * 1. Attempt to create record for following data
 *    - Missing, misspelled, extra attribute values
 *    - Very large values (which would fail if constrains are specified in requirement)
 *    - Blank, negative and alphanumeric values for salary, age fields
 *    - Floating point value for age
 * 2. Attempt to create a duplicate record
 * 
 * It uses @BeforeMethod and @AfterMethod (inherited from BaseTest) and @BeforeClass TestNG annotations to set up HTML reporting using ExtentReports
 * Each test method annotated by @Test contains test description, success and failure messages, api calls and assertions
 */

public class CreateTests extends BaseTest {
	public static final Logger LOG = Logger.getLogger(CreateTests.class);

	
	@BeforeClass
	public void setupCreateTestCases() {
		report = new ExtentReports(Paths.get("").toAbsolutePath().toString() + "\\src\\test\\resources\\results\\APICreateTestResults.html");
	}
		
	
	@Test(enabled=true, dataProvider="PositiveDataProvider")
	public void AddEmployeeTest(String testScenario, String employeeName, String employeeSalary, String employeeAge) {
		LOG.info("Running Scenario: " + testScenario);
		String employeeData = "{\"name\" : \"" + employeeName + "\", \"salary\" : \"" + employeeSalary + "\" , \"age\" : \"" + employeeAge + "\"}";
		
		reportLog.setDescription("Verify a new employee record is created successfully for " + testScenario + " case.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/create");
		SuccessMessage = "Successfully verified record creation for new employee for " + testScenario + " case.";
		FailureMessage = "Could not verify record creation with all valid details for " + testScenario + " case.";

             
        response = given().filter(new RequestLoggingFilter(requestCapture)).and().
        		header("contenetType", "application/json").and().body(employeeData).when().post("http://dummy.restapiexample.com/api/v1/create");
        
        AssertionCheck("Status Code = 200", response.statusCode() + "", "200");
        try {
        	AssertionCheck("Name = " + employeeName, response.body().jsonPath().get("name") + "", employeeName);
        	AssertionCheck("Salary = " + employeeSalary, response.body().jsonPath().get("salary") + "", employeeSalary);
        	AssertionCheck("Age = " + employeeAge, response.body().jsonPath().get("age") + "", employeeAge);
        } catch(JsonPathException e) {
        	reportLog.log(LogStatus.FAIL,  "Failed to parse JSON response. Response body does not contain JSON data.");
        	Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
        }
        
	}

	@Test(enabled=true, dataProvider="NegativeDataProvider")
	public void AddEmployeeDataNegativeTest(String testScenario, String errMsg, String employeeData) {
		LOG.info("Running Scenario: " + testScenario);
		reportLog.setDescription("Verify a new employee record is not created for " + testScenario + " case.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/create");
		SuccessMessage = "Successfully verified record is not created for new employee for " + testScenario + " case.";
		FailureMessage = "Could not verify API behavior when creating a record for " + testScenario + " case.";

             
        response = given().filter(new RequestLoggingFilter(requestCapture)).and().
        		header("contenetType", "application/json").and().body(employeeData).when().post("http://dummy.restapiexample.com/api/v1/create");
        
        AssertionCheck("Status Code = 200", response.statusCode() + "", "200");
        AssertionCheck("Response body contains '" + errMsg + "'", response.body().asString().contains(errMsg) + "", "true");
	}
	
	
	@Test(enabled=true)
	public void AddDuplicateEmployeeTest() {
		String employeeName = "TestUser" + System.currentTimeMillis();
		String employeeSalary = "30000";
		String employeeAge = "30";
		String employeeData = "{\"name\" : \"" + employeeName + "\", \"salary\" : \"" + employeeSalary + "\" , \"age\" : \"" + employeeAge + "\"}";
		
		reportLog.setDescription("Verify duplicate employee record is not created.");
		reportLog.log(LogStatus.INFO, "API End Point URL: http://dummy.restapiexample.com/api/v1/create");
		SuccessMessage = "Successfully verified duplicate record not created.";
		FailureMessage = "Could not verify API operation when trying to add duplicate record.";

         //Creating first record  
        given().filter(new RequestLoggingFilter(requestCapture)).and().
        		header("contenetType", "application/json").and().body(employeeData).when().post("http://dummy.restapiexample.com/api/v1/create");
        
        //Creating duplicate record
        response = given().filter(new RequestLoggingFilter(requestCapture)).and().
        		header("contenetType", "application/json").and().body(employeeData).when().post("http://dummy.restapiexample.com/api/v1/create");
        
        AssertionCheck("Status Code = 200", response.statusCode() + "", "200");
        AssertionCheck("Response body contains 'Duplicate entry'", response.body().asString().contains("Duplicate entry") + "", "true");
	}
	
	
	@DataProvider(name="PositiveDataProvider")
	public Object[][] SetPositiveData(){
		return new Object[][] { {"Average Values", "TestUser" + System.currentTimeMillis(), "30", "120000"},		 	// Valid Average Values
			{"Minimum Age Value", "TestUser" + System.currentTimeMillis() + Math.random(), "300000", "15"},            		// age 15
			{"Maximum Age Value", "TestUser" + System.currentTimeMillis() + Math.random(), "300000", "120"},          		// age 120 
			{"Minimum Salary Value", "TestUser" + System.currentTimeMillis() + Math.random(), "0", "30"},             	   	// salary 0  
			{"High Salary Value", "TestUser" + System.currentTimeMillis() + Math.random(), "3000000000", "30"},       		// salary 10 digits
			{"Floating Point Value For Salary", "TestUser" + System.currentTimeMillis() + Math.random(), "8000.20", "30"},	// salary floating point
			{"Name With Spaces Value", "Test User" + System.currentTimeMillis() + Math.random(), "300000", "30"},          	// name with spaces
			{"Name With Special Characters Value", "T#@st User" + System.currentTimeMillis() + Math.random(), "300000", "30"}, // name with special characters
			{"Name With Accent Marks Value", "T�t� User" + System.currentTimeMillis() + Math.random(), "300000", "30"},        // name with accent marks
			{"Name With Punctuation Marks Value", "T,t; U.e'r" + System.currentTimeMillis() + Math.random(), "300000", "30"}   // name with punctuation marks
		};
	}
	
	@DataProvider(name="NegativeDataProvider")
	public Object[][] SetNegativeData(){
		return new Object[][] { 
			{"Missing Name Attribute", "Integrity constraint violation", "{\"salary\" : \"3000000\", \"age\" : \"30\"}"}, 																				// missing name attribute
			{"Missing Salary Attribute", "Integrity constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"age\" : \"30\"}"}, 							// missing salary attribute
			{"Missing Age Attribute", "Integrity constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\"}"}, 					// missing age attribute
			{"Extra Attribute", "Integrity constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"department\" : \"sales\", \"salary\" : \"3000000\", \"age\" : \"30\"}"}, 																				// extra attribute
			{"Misspelled Attribute", "Integrity constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"celery\" : \"3000000\", \"age\" : \"30\"}"}, 																				// misspelled attribute
			{"Name Only","Integrity constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "}"}, 												// name only
			{"Salary Only","Integrity constraint violation", "{\"salary\" : \"3000000\"}"}, 																								// salary only
			{"Age Only", "Integrity constraint violation", "{\"age\" : \"30\"}"}, // age only
			{"Very Large Name Value", "Data constraint violation", "{\"name\" : \"TestUser12345678901234567890123456789012345678901234567890123456789012345678901234567890" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"30\"}"},
			{"Very Large Salary Value", "Data constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"12345678901234567890123456789012345678901234567890123456789012345678901234567890\", \"age\" : \"30\"}"},
			{"Very Large Age VAlue", "Data constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"12345678901234567890123456789012345678901234567890123456789012345678901234567890\"}"},
			{"Blank Value For Salary", "Data constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"\", \"age\" : \"30\"}"},			// salary blank
			{"Negative Value For Salaray", "Data constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"-2000\", \"age\" : \"30\"}"},		// salary negative
			{"Alphanumeric Value For Salary", "Data constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"abc\", \"age\" : \"30\"}"},		// salary alphanumeric
			{"Blank Value For Age", "Data constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"\"}"},		// age blank
			{"Negative Value For Age", "Data constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"-20\"}"},	// age negative
			{"Floating Point Value For Age", "Data constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"13.5\"}"},	// age floating point
			{"Alphanumeric Value For Age", "Data constraint violation", "{\"name\" : \"TestUser" + System.currentTimeMillis() + Math.random() + "\", \"salary\" : \"300000\", \"age\" : \"pqr\"}"} 	// age alphanumeric
							
		};
	}
	
}
